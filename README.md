In caesar.py, the given text file is rotated the amount specified, 
or without any specification is rotated using values from 0-25.

The grader will need to supply input along the lines of 
"python src/caesar.py data/rot0.txt 5" on the command line.

I have not found any issues of input besides the intended errors as 
found in the assignment description.
