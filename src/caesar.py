import sys

#A-Z = 65-90
#a-z = 97-122


#checks for how many arguments are supplied, if less than 2 prints usage
if len(sys.argv) < 2:
    print("Usage: " + sys.argv[0] + " FILENAME [ROTATION]")
    print("When no rotation distance is specified all 26 possible rotations are tried")

#if more than 2 arguments, opens text file cited in 2nd arg
elif len(sys.argv) == 2:
    text = open(str(sys.argv[1]), "r")
    i=0
    while i<26:
        text = open(str(sys.argv[1]), "r")
        print("===============================")
        print("Rotated by " + str(i) + " positions")
        print("===============================")
        for line in text:
            for ch in line:
                if ord(ch)<65 or ord(ch)>90 and ord(ch)<97 or ord(ch)>122:
                    print(ch, end = "")
                elif ch.islower() and ord(ch)+i>122:
                    diff = 122-ord(ch)
                    diff2 = i - diff
                    print(chr(96+diff2), end = "")
                elif ch.isupper() and ord(ch)+i > 90:
                    diff = 90-ord(ch)
                    diff2 = i - diff
                    print(chr(64+diff2), end = "")
                else:
                    print(chr(ord(ch)+i), end = "")
        i+=1


elif len(sys.argv)==3:
    text = open(str(sys.argv[1]), "r")
    number = int(sys.argv[2])
    if number < 0 or number > 25:
        print("Error: rotation number must be between 0 and 25")
    else:
        print("===============================")
        print("Rotated by " + str(number) + " positions")
        print("===============================")
        for line in text:
            for ch in line:
                if ord(ch)<65 or ord(ch)>90 and ord(ch)<97 or ord(ch)>122:
                    print(ch, end = "")
                elif ch.islower() and ord(ch)+number>122:
                    diff = 122-ord(ch)
                    diff2 = number - diff
                    print(chr(96+diff2), end = "")
                elif ch.isupper() and ord(ch)+number > 90:
                    diff = 90-ord(ch)
                    diff2 = number - diff
                    print(chr(64+diff2), end = "")
                else:
                    print(chr(ord(ch)+number), end = "")
